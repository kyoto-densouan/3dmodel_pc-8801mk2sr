# README #

1/3スケールのNEC PC-8801mk2SR風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- NEC

## 発売時期
- PC-8801mk2SR:1985年
- PC-8801mk2FR:1985年
- PC-8801mk2MR:1985年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-8800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801mk2sr/raw/d9fca549d02c91daf4d6f6b18edbf94f534e4c90/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801mk2sr/raw/d9fca549d02c91daf4d6f6b18edbf94f534e4c90/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-8801mk2sr/raw/d9fca549d02c91daf4d6f6b18edbf94f534e4c90/ExampleImage.png)
